# FluentMigrator Migrations

Visual Studio extension that an empty FluentMigator Migration with the migration timestamp in the filename.

The migration file that is created is like

```csharp
using FluentMigrator;

[Migration(20140401073254)]
public class _20140401073254_Migration2 : Migration
{
    public override void Up()
    {

    }

    public override void Down()
    {

    }
}
```    


and the file name is `20140401073254_Migration2.cs`

## To Install
The extension only supports VS 2012 and .NET 4.5.
Download and run the VSIX from the downloads page.

## To Create a Migration
In VS, add a new item (right click on the project or folder and select add -> new item).
Under the Visual C# Items, you should find FluentMigrator.Migration template.